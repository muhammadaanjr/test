<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'M@qdLyj7i,y@3Zd:DZ` $k*]k]YB{lxPKz%e8?mE5>~tQpu:*ojl*Z{Nivj&3MBa');
define('SECURE_AUTH_KEY',  'h[wrdeug`UNtHS%t,)qkQ`W;vr*]vTU $>6dKziEFh<6QIJSL`r]>z^p08V2/[@F');
define('LOGGED_IN_KEY',    '~Tj3dtAQ4tQrNHuXmP^G|`NHJ UQ,*[T%juz5>5`,j</3|3O[h4^[#*KN>ineLx%');
define('NONCE_KEY',        '<gVf25})XZib@e70thTC:A(,P8)J+%%Zv8rOo;3c5J%.syu(twecT9$aCo4?+uo^');
define('AUTH_SALT',        'VU!HtI9v/ly=FKE*sHk$pjo*p+#gf_Q@=#R OI~9OI-M&Go0Pml7]A{oyw]DEA:@');
define('SECURE_AUTH_SALT', '+7~pIKsncphNF58,mFRs e1{S_2eYYNmzo}7&`6j;e?!F4#%F<=:uiqPr@4|j+C8');
define('LOGGED_IN_SALT',   'c5PiE ],5Osn2;GJ`Y$9Mm7Ceydgw219a-q.)sky~#Yt+5UP,?Zsz,S~1l~v.!RI');
define('NONCE_SALT',       '):c$LVzB2>Q/lB9$B~yMlkV@5:/DNY&9|o#,q%jd4IRck9`BhMFDUV/Sl5Ma|;ag');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
