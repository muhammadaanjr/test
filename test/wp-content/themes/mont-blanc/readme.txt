MontBlanc WordPress theme, Copyright (C) 2014 Titouanc
MontBlanc is distributed under the terms of the GNU GPL

MontBlanc uses wp-bootstrap-navwalker https://github.com/twittem/wp-bootstrap-navwalker, licensed under the GPL license. Copyright (C) Twittem, https://github.com/twittem

MontBlanc uses Respond.js https://github.com/scottjehl/Respond, licensed under the MIT license. Copyright (C) Scott Jehl, scottjehl.com

MontBlanc uses html5shim https://code.google.com/p/html5shim/, dual licensed under the MIT or GPL Version 2 licenses. Copyright (C) Remy Sharp, https://twitter.com/rem

MontBlanc uses Twitter Bootstrap  https://getbootstrap.com, licensed under the MIT license. Copyright (C) Mark Otto - https://twitter.com/mdo, Jacob - https://twitter.com/fat

MontBlanc uses Font Awesome  http://fortawesome.github.io/Font-Awesome/, licensed under the MIT license and under the SIL Open Font License. Copyright (C) Dave Gandy https://twitter.com/davegandy

MontBlanc uses Roots Walker Comment from the Roots Starter Theme, licensed under a GPL compatible licensed (see license here https://github.com/roots/roots/blob/master/LICENSE.md). Copyright (c) Ben Word and Scott Walkinshaw. 

The image used in the theme sccreenshot is licensed under the CC0 license.