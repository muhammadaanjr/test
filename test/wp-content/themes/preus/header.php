<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Preus
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="parallax-bg"></div>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	
	<div id="top-bar">
	<div class="container">
	
	<div id="top-search" class="col-lg-4 col-md-12 col-sm-12">
	<?php get_search_form(); ?>
	</div>	
	
	 <div class="default-nav-wrapper col-md-12 col-lg-8 col-xs-12 col-sm-12"> 	
	   <div class="nav-wrapper container">
		   <nav id="site-navigation" class="navbar navbar-default main-navigation" role="navigation">
			
			<div class="navbar-header">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		      <span class="sr-only">Toggle navigation</span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		    </button>
		  </div>
		  
			
			<?php
			    wp_nav_menu( array(
			        'theme_location'    => 'top',
			        'depth'             => 2,
			        'container'         => 'div',
			        'container_class'   => 'collapse navbar-collapse navbar-ex1-collapse',
			        'menu_class'        => 'nav navbar-nav',
			        'fallback_cb'       => false,
			        'walker'            => new wp_bootstrap_navwalker())
			    );
?>
		 </nav><!-- #site-navigation -->
		</div>
	  </div>
	
	</div>
	</div><!--#top-bar-->
	<header id="masthead" class="site-header row container" role="banner">
		<div class="site-branding col-md-12">
		<?php if((of_get_option('logo', true) != "") && (of_get_option('logo', true) != 1) ) { ?>
			<h1 class="site-title logo-container"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
			<?php
			echo "<img class='main_logo' src='".of_get_option('logo', true)."' title='".esc_attr(get_bloginfo( 'name','display' ) )."'></a></h1>";	
			}
		else { ?>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> 
			<h2 class="site-description"><?php bloginfo('description') ?></h2>
		<?php	
		}
		?>
		</div>
	</header><!-- #masthead -->
	
	
	<div id="content" class="site-content row">
	
	<?php get_template_part('carousel'); ?>
		
	<div id="primary-nav-wrapper" class="col-md-12 container">
				<nav id="primary-navigation" class="primary-navigation" role="navigation">
					
						<h1 class="menu-toggle"><?php _e( 'Menu', 'sixteen' ); ?></h1>
						<div class="screen-reader-text skip-link"><a href="#content"><?php _e( 'Skip to content', 'sixteen' ); ?></a></div>
			
						<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
					
				</nav><!-- #site-navigation -->
		</div>	
		<div class="container col-md-12"> 
