<?php
	if ( ( of_get_option('carousel_enabled') != 0 ) && ( (is_home() == true) || (is_front_page() ==true) ) )  
		{ ?>
    <div id="carousel-wrapper" class="col-md-12 container clearfix">
    <ul class="bxslider">
    	<?php
    		$args = array( 'posts_per_page' => of_get_option('carousel_count'), 'category' => of_get_option('carousel_category') );
			$lastposts = get_posts( $args );
			foreach ( $lastposts as $post ) :
			  setup_postdata( $post ); ?>
			  	<li><a title="<?php the_title(); ?>" href='<?php the_permalink(); ?>'>
			  	<?php if (has_post_thumbnail()) : 
					$thumb_id = get_post_thumbnail_id();
					$thumb_url = wp_get_attachment_image_src($thumb_id,'carousel-thumb', true);
					echo "<img class='carousel-image' src='".$thumb_url[0]."' title='".get_the_title()."'>";	
		else :
			echo "<img class='carousel-image' src='".get_template_directory_uri()."/images/cthumb.png' title='".get_the_title()."'>";	
	endif; ?></a></li>
			<?php endforeach; 
			wp_reset_postdata(); 
			?>			
     </ul>   
	</div>
    <?php } ?>
