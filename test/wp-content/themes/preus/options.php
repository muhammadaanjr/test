<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = wp_get_theme();
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

function optionsframework_options() {

	$options = array();
	$imagepath =  get_template_directory_uri() . '/images/';	
	$true_false = array(
		'true' => __('true', 'preus'),
		'false' => __('false', 'preus')
	);	
	// Get all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	$carousel_count = array(
		'4' => __('4', 'preus'),
		'5' => __('5', 'preus'),
		'6' => __('6', 'preus'),
		'7' => __('7', 'preus'),
		'8' => __('8', 'preus'),
		'9' => __('9', 'preus'),
		'10' => __('10', 'preus'),
		'11' => __('11', 'preus'),
		'12' => __('12', 'preus'),
	);
	
	//Basic Settings
	
	$options[] = array(
		'name' => __('Basic Settings', 'preus'),
		'type' => 'heading');
			
	$options[] = array(
		'name' => __('Site Logo', 'preus'),
		'desc' => __('Leave Blank to use text Heading.', 'preus'),
		'id' => 'logo',
		'class' => '',
		'type' => 'upload');
		
	$options[] = array(
		'desc' => __('For More Cuztomization Options including Favicon, Styles, Header/Footer Scripts etc., <a target="_blank" href="http://inkhive.com/product/preus-plus/">Upgrade to Pro Now.</a> ', 'preus'),
		'type' => 'info');	
		
	$options[] = array(
		'name' => __('Enable Excerpts on Homepage', 'preus'),
		'desc' => __('By default, the theme shows either the full post or content up till the point where you placed the &lt;!--more--> tag. Check this if you want to you enable Excerpts on Homepage. Excerpts are short summary of your posts.', 'preus'),
		'id' => 'excerpt1',
		'std' => '0',
		'type' => 'checkbox');	

	$options[] = array(
		'name' => __('Copyright Text', 'preus'),
		'desc' => __('Some Text regarding copyright of your site, you would like to display in the footer.', 'preus'),
		'id' => 'footertext2',
		'std' => '',
		'type' => 'text');
		
	//Design Settings
		
	$options[] = array(
		'name' => __('Layout Settings', 'preus'),
		'type' => 'heading');	
	
	$options[] = array(
		'name' => "Sidebar Layout",
		'desc' => "Select Layout for Posts & Pages.",
		'id' => "sidebar-layout",
		'std' => "right",
		'type' => "images",
		'options' => array(
			'left' => $imagepath . '2cl.png',
			'right' => $imagepath . '2cr.png')
	);
	
	$options[] = array(
		'desc' => __('With Preus Plus You can change the color scheme of the entire theme. You can choose from Unlimited colors. <a target="_blank" href="http://inkhive.com/product/preus-plus/">Upgrade to Pro Now.</a> ', 'preus'),
		'type' => 'info');	
	
	$options[] = array(
		'name' => __('Custom CSS', 'preus'),
		'desc' => __('Some Custom Styling for your site. Place any css codes here instead of the style.css file.', 'preus'),
		'id' => 'style2',
		'std' => '',
		'type' => 'textarea');
	
	//CAROUSEL SETTINGS
	$options[] = array(
		'name' => __('Carousel Settings', 'preus'),
		'type' => 'heading');	

	$options[] = array(
		'name' => __('Enable Carousel', 'preus'),
		'desc' => __('Check this to Enable Carousel', 'preus'),
		'id' => 'carousel_enabled',
		'type' => 'checkbox',
		'std' => '0' );	
		
	if ( $options_categories ) {
	$options[] = array(
		'name' => __('Select Featured Category', 'preus'),
		'desc' => __('The category from which posts will be fetched', 'preus'),
		'id' => 'carousel_category',
		'type' => 'select',
		'options' => $options_categories);
	}
	
	$options[] = array(
		'name' => __('Select No of Posts', 'preus'),
		'desc' => __('The no. of posts you want to display in the carousel.', 'preus'),
		'id' => 'carousel_count',
		'std' => '6',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => $carousel_count);


	//SLIDER SETTINGS

	$options[] = array(
		'name' => __('Slider Settings', 'preus'),
		'type' => 'heading');

	$options[] = array(
		'desc' => __('Slider is Available in the pro version only. <a target="_blank" href="http://inkhive.com/product/preus-plus/">Buy Preus Plus Now.</a> ', 'preus'),
		'type' => 'info');	
			
	//Social Settings
	
	$options[] = array(
	'name' => __('Social Settings', 'preus'),
	'type' => 'heading');

	$options[] = array(
		'name' => __('Facebook', 'preus'),
		'desc' => __('Facebook Profile or Page URL i.e. http://facebook.com/username/ ', 'preus'),
		'id' => 'facebook',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Twitter', 'preus'),
		'desc' => __('Twitter Username', 'preus'),
		'id' => 'twitter',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
	
	$options[] = array(
		'name' => __('Google Plus', 'preus'),
		'desc' => __('Google Plus profile url, including "http://"', 'preus'),
		'id' => 'google',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Feeburner', 'preus'),
		'desc' => __('URL for your RSS Feeds', 'preus'),
		'id' => 'feedburner',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');	
		
	$options[] = array(
		'name' => __('Pinterest', 'preus'),
		'desc' => __('Your Pinterest Profile URL', 'preus'),
		'id' => 'pinterest',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Instagram', 'preus'),
		'desc' => __('Your Instagram Profile URL', 'preus'),
		'id' => 'instagram',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');	
		
	$options[] = array(
		'name' => __('Linked In', 'preus'),
		'desc' => __('Your Linked In Profile URL', 'preus'),
		'id' => 'linkedin',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');	
		
	$options[] = array(
		'name' => __('Youtube', 'preus'),
		'desc' => __('Your Youtube Channel URL', 'preus'),
		'id' => 'youtube',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Tumblr', 'preus'),
		'desc' => __('Your Tumblr Blog URL', 'preus'),
		'id' => 'tumblr',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Flickr', 'preus'),
		'desc' => __('Your Flickr Profile URL', 'preus'),
		'id' => 'flickr',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Dribbble', 'preus'),
		'desc' => __('Your Dribble Profile URL', 'preus'),
		'id' => 'dribble',
		'std' => '',
		'class' => 'mini',
		'type' => 'text');	
		
	$options[] = array(
		'desc' => __('Preus Plus Has More Social Icons. We can add any custom social icon on request. <a target="_blank" href="http://inkhive.com/product/preus-plus/">Read Features and Buy Preus Plus.</a> ', 'preus'),
		'type' => 'info');								
		
	$options[] = array(
	'name' => __('Support', 'preus'),
	'type' => 'heading');
	
	$options[] = array(
		'desc' => __('Preus WordPress theme has been Designed and Created by <a href="http://InkHive.com" target="_blank">InkHive.com</a>. For any Queries or help regarding this theme, <a href="http://wordpress.org/support/theme/preus" target="_blank">use the support forums.</a>', 'preus'),
		'type' => 'info');	
	
	$options[] = array(
		'desc' => __('A Documentation file has been provided with the theme, for your convenience. <a target="_blank" href="'.get_template_directory_uri().'/Documentation-Preus.pdf">Preus Theme Documentation.</a> ', 'preus'),
		'type' => 'info');	
		
	$options[] = array(
		'desc' => __('Pro Version Comes with Dedicated Support and personal e-mail support. <a target="_blank" href="http://inkhive.com/product/preus-plus/">Upgrade to Pro Now.</a> ', 'preus'),
		'type' => 'info');	
		
	 $options[] = array(
		'desc' => __('<a href="http://twitter.com/rohitinked" target="_blank">Follow Me on Twitter</a> to know about my upcoming themes.', 'preus'),
		'type' => 'info');
		
	$options[] = array(
		'name' => __('Live Demo Blog', 'preus'),
		'desc' => __('For your convenience, we have created a <a href="http://demo.inkhive.com/preus/" target="_blank">Live Demo Blog</a> of the theme Preus. You can take a look at and find out how your site would look once complete.', 'preus'),
		'type' => 'info');			
	
	$options[] = array(
		'name' => __('Regenerating Post Thumbnails', 'preus'),
		'desc' => __('If you are using preus Theme on a New Wordpress Installation, then you can skip this section.<br />But if you have just switched to this theme from some other theme, then you are requested regenerate all the post thumbnails. It will fix all the issues you are facing with distorted & ugly homepage thumbnail Images. ', 'preus'),
		'type' => 'info');	
		
	$options[] = array(
		'desc' => __('To Regenerate all Thumbnail images, Install and Activate the <a href="http://wordpress.org/plugins/regenerate-thumbnails/" target="_blank">Regenerate Thumbnails</a> WP Plugin. Then from <strong>Tools &gt; Regen. Thumbnails</strong>, re-create thumbnails for all your existing images. And your blog will look even more stylish with Preus theme.<br /> ', 'preus'),
		'type' => 'info');	
		
			
	$options[] = array(
		'desc' => __('<strong>Note:</strong> Regenerating the thumbnails, will not affect your original images. It will just generate a separate image file for those images.', 'preus'),
		'type' => 'info');	
		
	
	$options[] = array(
		'name' => __('Theme Credits', 'preus'),
		'desc' => __('Check this if you want to you do not want to give us credit in your site footer.', 'preus'),
		'id' => 'credit1',
		'std' => '0',
		'type' => 'checkbox');

	$options[] = array(
		'name' => __('Upgrade to Pro', 'preus'),
		'type' => 'heading');
			
	$options[] = array(
		'desc' => __('Pro Version of Preus Plus Comes with a Huge Set of Features inlcuding Custom Widgets, Full Width Responsive Slider, Support for Older Browsers, A Custom Business/Corporate page, etc. <a target="_blank" href="http://inkhive.com/product/preus-plus/">Read all features and Upgrade to Preus Plus.</a> ', 'preus'),
		'type' => 'info');	
	
	

	return $options;
}