<?php
/**
 * Preus Theme Customizer
 *
 * @package Preus
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function preus_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->remove_control( 'header_textcolor');
	$wp_customize->remove_control( 'background_color');
	
	$wp_customize->add_setting( 'preus_bg_color', array (
			'default'	=> '#ffffff',
		) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'preus_bg_color', array(
			'label'    => __( 'Background Color', 'preus' ),
			'section'  => 'colors',
			'settings' => 'preus_bg_color',
			'priority'    => 101,
	) ) );
	
	$wp_customize->add_setting( 'preus_title_color', array (
			'default'	=> '#00ac97',
		) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'preus_title_color', array(
			'label'    => __( 'Site Title Color', 'preus' ),
			'section'  => 'colors',
			'settings' => 'preus_title_color',
			'priority'    => 102,
	) ) );
	
	$wp_customize->add_setting( 'preus_desc_color', array (
			'default'	=> '#b4b4b4',
		) );
	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'preus_desc_color', array(
			'label'    => __( 'Site Description Color', 'preus' ),
			'section'  => 'colors',
			'settings' => 'preus_desc_color',
			'priority'    => 102,
	) ) );
	
		
}
add_action( 'customize_register', 'preus_customize_register' );

if ( ! function_exists( 'preus_apply_color' ) ) :
  function preus_apply_color() { ?>
  		<style id="preus-custom-style">
  		<?php if (get_theme_mod('preus_desc_color') ) : ?>
  			.site-description { color: <?php echo get_theme_mod('preus_desc_color') ?> }
  		<?php endif; ?>
  		<?php if (get_theme_mod('preus_title_color') ) : ?>
  			.site-title a { color: <?php echo get_theme_mod('preus_title_color') ?> }
  		<?php endif; ?>
  		<?php if (get_theme_mod('preus_bg_color') ) : ?>
  			#content { background-color: <?php echo get_theme_mod('preus_bg_color') ?>; background-image: url(<?php echo background_image() ?>); background-repeat: <?php echo get_theme_mod('background_repeat') ?>; background-position: top <?php echo get_theme_mod('background_position_x') ?>; background-attachment: <?php echo get_theme_mod('background_attachment') ?>; }
  		<?php endif; ?>

  		</style>
  	
  <?php 	
  }
  endif;

add_action( 'wp_head', 'preus_apply_color' );
/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function preus_customize_preview_js() {
	wp_enqueue_script( 'preus_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'preus_customize_preview_js' );
