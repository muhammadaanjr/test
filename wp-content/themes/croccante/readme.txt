=== Croccante ===
Contributors: CrestaProject
Tags: two-columns, right-sidebar, custom-colors, custom-menu, featured-images, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready, blog, news, e-commerce, footer-widgets, custom-logo
Requires at least: 4.5
Tested up to: 4.8
Stable tag: 1.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Croccante is a simple and fresh multipurpose WordPress Theme with a lot of features to create your own website. With the “OnePage” template you can use sections to create your own page (Slider, About Us, Team Section, Skills, Contact). Also, Croccante theme is excellent for blogging website with a clean design and simple structure. Croccante Theme is fully compatible with WooCommerce that allows you to create your personal E-Commerce and with WPML (multilingual ready). Unlimited colos, 2 sidebars, footer widget, custom logo, social buttons and many other options.

== Frequently Asked Questions ==
= Does this theme support any plugins? =

Croccante includes support for Infinite Scroll in Jetpack, WooCommerce and WPML.

== Credits ==
* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
* FontAwesome (http://fontawesome.io/) Font licensed under SIL OFL 1.1 and Code lisensed under MIT
* FlexSlider (https://github.com/woocommerce/FlexSlider) licensed under GPL2
* nanoScrollerJS (https://github.com/jamesflorentino/nanoScrollerJS) licensed under MIT
* Waypoints (https://github.com/imakewebthings/waypoints) licensed under MIT
* SmoothScroll (https://github.com/galambalazs/smoothscroll-for-websites) licensed under MIT
* Image used in Theme Screenshot (https://www.pexels.com/photo/man-wearing-black-hat-a-bow-and-a-sweat-shirt-on-a-backstage-175696/) Pexels Free photos for commercial and personal works

== Changelog ==

= Version 1.0.3 =
* Minor bug fixes

= Version 1.0.2 =
* Fixed a bug with the WooCommerce template
* Minor bug fixes

= Version 1.0.1 =
* Improved WPML compatibility
* Improved the comments section
* Minor bug fixes

= Version 1.0.0 =
* Initial release
