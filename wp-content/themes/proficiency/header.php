<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package proficiency
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>
<?php $theme_layout = get_theme_mod('proficiency_theme_layout_options','wide'); ?>

<?php 
	$theme_layout = get_theme_mod('proficiency_theme_layout_options','wide');
	if($theme_layout == "boxed")
	{ $class="boxed"; }
	else
	{ $class="wide"; }
 ?>
<body <?php body_class($class); ?> >
<div class="wrapper">
<a style="display:none;" class="skip-link screen-reader-text" href="#content">
<?php esc_html_e( 'Skip to content', 'proficiency' ); ?>
</a>
<header> 
  <!--==================== TOP BAR ====================-->
  <div class="proficiency-head-detail hidden-xs hidden-sm">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 col-xs-12 col-sm-6">
          <ul class="info-left">
            <?php 
              $proficiency_head_info_one = get_theme_mod('proficiency_head_info_one','<a><i class="fa fa-clock-o "></i>Open-Hours:10 am to 7pm</a>','proficiency');
              $proficiency_head_info_two = get_theme_mod('proficiency_head_info_two','<a href="mailto:info@themeansar.com" title="Mail Me"><i class="fa fa-envelope"></i> info@themeansar.com</a>','proficiency');
            ?>
            <li><?php echo $proficiency_head_info_one; ?></li>
            <li><?php echo $proficiency_head_info_two; ?></li>
          </ul>
        </div>
        <div class="col-md-6 col-xs-12 col-sm-6">
        <?php 
              wp_nav_menu( 
              array(  
              'theme_location'  => 'top',
              'container'     => '',
              'menu_class'    => 'info-right',
              'fallback_cb'     => 'proficiency_custom_navwalker',
              'walker'      => new proficiency_custom_navwalker()
            ) );
          
        ?>
          </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="proficiency-main-nav">
    <nav class="navbar navbar-default navbar-wp">
      <div class="container-fluid">
        <div class="navbar-header"> 
          
          <!-- Logo -->
          <?php
          $site_enable_logo = get_theme_mod('enable_logo','true');
          $site_logo = get_theme_mod('header_logo_image',get_template_directory_uri() . '/images/logo.png');
          $site_text = get_theme_mod('enable_header_logo_text','true');
          $site_logo_width  = get_theme_mod('logo_width','');
          $site_logo_height = get_theme_mod('logo_height','');

          if($site_enable_logo == true) {

            if($site_text == false) {
               echo '<a href="'.esc_url( home_url( '/' ) ).'" class="navbar-brand">';
                  echo  get_bloginfo();
                  echo '<br>';
                  echo '<span class="site-description">';
                  echo  get_bloginfo( 'description', 'display' ); /* WPCS: xss ok. */ 
                        '</span>';
              echo '</a>';
            }

            elseif ($site_logo!= '') {

              echo '<a href="'.esc_url( home_url( '/' ) ).'" class="navbar-brand">'; ?>

                <img src="<?php echo esc_url( $site_logo ) ;?>" style="height:<?php echo $site_logo_height;?>px; width:<?php echo $site_logo_width; ?>px;"  alt="<?php echo esc_attr( get_bloginfo('title') ); ?>">

              <?php
              echo '</a>';
            } 
          } ?>

          <!-- navbar-toggle -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-wp"> <span class="sr-only"><?php echo 'Toggle Navigation'; ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <!-- /navbar-toggle --> 
        
        <!-- Navigation -->
        <div class="collapse navbar-collapse" id="navbar-wp">
          <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav navbar-right', 'fallback_cb' => 'proficiency_custom_navwalker::fallback' , 'walker' => new proficiency_custom_navwalker() ) ); ?>
        </div>
        <!-- /Navigation --> 
      </div>
    </nav>
  </div>
</header>
<!-- #masthead --> 